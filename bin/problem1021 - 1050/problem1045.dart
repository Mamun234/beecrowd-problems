import 'dart:io';

void main() {
  double a, b, c;

  List<String> inputValues = stdin.readLineSync()!.split(" ");
  a = double.parse(inputValues[0]);
  b = double.parse(inputValues[1]);
  c = double.parse(inputValues[2]);

// Sort in decreasing order
  List<double> triangleSides = [a, b, c];
  triangleSides.sort(
    (a, b) => b.compareTo(a),
  );
  //print(triangleSides);

  a = triangleSides[0];
  b = triangleSides[1];
  c = triangleSides[2];

  if (a >= (b + c)) {
    print("NAO FORMA TRIANGULO");
  } else {
    if ((a * a) == (b * b) + (c * c)) {
      print("TRIANGULO RETANGULO");
    } else if ((a * a) > (b * b) + (c * c)) {
      print("TRIANGULO OBTUSANGULO");
    } else if ((a * a) < (b * b) + (c * c)) {
      print("TRIANGULO ACUTANGULO");
    }

    if (a == b && b == c) {
      print("TRIANGULO EQUILATERO");
    } else if ((a == b && b != c) || (a == c && c != b) || (b == c && c != a)) {
      print("TRIANGULO ISOSCELES");
    }
  }
}
