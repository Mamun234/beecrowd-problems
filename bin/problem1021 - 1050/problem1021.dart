import 'dart:io';

void main() {
  double n;
  List<int> notes = [100, 50, 20, 10, 5, 2];
  List<double> coins = [1, 0.50, 0.25, 0.10, 0.05, 0.01];

  n = double.parse(stdin.readLineSync()!);
//convart input double n to int intN becous modulas not working double value
  int intN = (n * 100).toInt();

  print("NOTAS:");
  for (int denomination in notes) {
    int count = intN ~/ (denomination * 100);
    if (count > -1) {
      print("$count nota(s) de R\$ ${denomination.toStringAsFixed(2)}");

      intN = intN % (denomination * 100);
    }
  }

  print("MOEDAS:");
  for (double denomination in coins) {
    int count = intN ~/ (denomination * 100);
    if (count > -1) {
      print("$count moeda(s) de R\$ ${denomination.toStringAsFixed(2)}");

      intN = intN % (denomination * 100).toInt();
    }
  }
}
