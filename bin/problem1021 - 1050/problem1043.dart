import 'dart:io';

void main() {
  double a, b, c, perimeter, area;

  List<String> inputValues = stdin.readLineSync()!.split(" ");
  a = double.parse(inputValues[0]);
  b = double.parse(inputValues[1]);
  c = double.parse(inputValues[2]);

  if (a < (b + c) && b < (a + c) && c < (a + b)) {
    perimeter = a + b + c;
    print("Perimetro = ${perimeter.toStringAsFixed(1)}");
  } else {
    area = ((a + b) * c) / 2;
    print("Area = ${area.toStringAsFixed(1)}");
  }
}
