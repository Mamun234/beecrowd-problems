import 'dart:io';

void main() {
  double x, y;

  List<String> inputValues = stdin.readLineSync()!.split(" ");
  x = double.parse(inputValues[0]);
  y = double.parse(inputValues[1]);

  if (x == 0.0 && y == 0.0) {
    print("Origem");
  } else if (x == 0) {
    print("Eixo Y");
  } else if (y == 0) {
    print("Eixo X");
  } else if (x > 0 && y > 0) {
    print("Q1");
  } else if (x < 0 && y < 0) {
    print("Q3");
  } else if (x > 0 && y < 0) {
    print("Q4");
  } else if (x < 0 && y > 0) {
    print("Q2");
  }
}
