import 'dart:io';

void main() {
  int a, b, c, d;

  List<String> inputValues = stdin.readLineSync()!.split(" ");
  a = int.parse(inputValues[0]);
  b = int.parse(inputValues[1]);
  c = int.parse(inputValues[2]);
  d = int.parse(inputValues[3]);

  if (b > c && d > a && (c + d) > (a + b) && c > 0 && d > 0 && a % 2 == 0) {
    print("Valores aceitos");
  } else {
    print('Valores nao aceitos');
  }
}
