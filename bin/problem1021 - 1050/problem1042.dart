import 'dart:io';

void main() {
  // Read three integers
  List<String> inputs = stdin.readLineSync()!.split(" ");

//parse string to int
  List<int> numbers = inputs.map((e) => int.parse(e)).toList();
//sorted asendind
  List<int> sortedNumbers = List.from(numbers)..sort();
  //or sortedNumbers.sort();

  for (int num in sortedNumbers) {
    print("$num");
  }

// Print a blank line
  print('');

  for (int num in numbers) {
    print("$num");
  }
}
