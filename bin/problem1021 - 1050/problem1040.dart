import 'dart:io';

void main() {
  double n1, n2, n3, n4, n5, average, finalAverage;

  List<String> inputValues = stdin.readLineSync()!.split(" ");
  n1 = double.parse(inputValues[0]);
  n2 = double.parse(inputValues[1]);
  n3 = double.parse(inputValues[2]);
  n4 = double.parse(inputValues[3]);

  average = ((n1 * 2) + (n2 * 3) + (n3 * 4) + (n4 * 1)) / (2 + 3 + 4 + 1);

  print("Media: ${average.toStringAsFixed(1)}");

  if (average >= 7.0) {
    print("Aluno aprovado.");
  } else if (average < 5.0) {
    print("Aluno reprovado.");
  } else if (average >= 5.0 && average <= 6.9) {
    print("Aluno em exame.");

    n5 = double.parse(stdin.readLineSync()!);
    print("Nota do exame: $n5");

    finalAverage = (average + n5) / 2;
    if (finalAverage >= 5.0) {
      print("Aluno aprovado.");
      print("Media final: ${finalAverage.toStringAsFixed(1)}");
    } else if (finalAverage <= 4.9) {
      print("Aluno reprovado.");
      print("Media final: ${finalAverage.toStringAsFixed(1)}");
    }
  }
}
