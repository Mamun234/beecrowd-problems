import 'dart:io';

void main() {
  String w1, w2, w3;

  w1 = stdin.readLineSync()!;
  w2 = stdin.readLineSync()!;
  w3 = stdin.readLineSync()!;

  if (w1 == "vertebrado" || w1 == "invertebrado") {
    if (w2 == "ave") {
      if (w3 == "carnivoro") {
        print("aguia");
      } else if (w3 == "onivoro") {
        print("pomba");
      }
    } else if (w2 == "mamifero") {
      if (w3 == "onivoro") {
        print("homem");
      } else if (w3 == "herbivoro") {
        print("vaca");
      }
    } else if (w2 == "inseto") {
      if (w3 == "hematofago") {
        print("pulga");
      } else if (w3 == "herbivoro") {
        print("lagarta");
      }
    } else if (w2 == "anelideo") {
      if (w3 == "hematofago") {
        print("sanguessuga");
      } else if (w3 == "onivoro") {
        print("minhoca");
      }
    }
  }
}
