import 'dart:io';
import 'dart:math';

void main() {
  double a, b, c, cal, r1, r2;

  List<String> inputValues = stdin.readLineSync()!.split(" ");
  a = double.parse(inputValues[0]);
  b = double.parse(inputValues[1]);
  c = double.parse(inputValues[2]);

  cal = (b * b) - (4 * a * c);

  if (a == 0 || cal < 0) {
    print("Impossivel calcular");
  } else {
    r1 = (-b + sqrt(cal)) / (2 * a);
    r2 = (-b - sqrt(cal)) / (2 * a);

    print("R1 = ${r1.toStringAsFixed(5)}");
    print("R2 = ${r2.toStringAsFixed(5)}");
  }
}
