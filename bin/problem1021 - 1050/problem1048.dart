import 'dart:io';

void main() {
  double salary, moneyEarned, newSalary;
  salary = double.parse(stdin.readLineSync()!);

  if (salary > 0 && salary <= 400.00) {
    moneyEarned = (salary * 15) / 100;
    newSalary = salary + moneyEarned;

    print("Novo salario: ${newSalary.toStringAsFixed(2)}");
    print("Reajuste ganho: ${moneyEarned.toStringAsFixed(2)}");
    print("Em percentual: 15 %");
  } else if (salary >= 400.01 && salary <= 800.00) {
    moneyEarned = (salary * 12) / 100;
    newSalary = salary + moneyEarned;

    print("Novo salario: ${newSalary.toStringAsFixed(2)}");
    print("Reajuste ganho: ${moneyEarned.toStringAsFixed(2)}");
    print("Em percentual: 12 %");
  } else if (salary >= 800.01 && salary <= 1200.00) {
    moneyEarned = (salary * 10) / 100;
    newSalary = salary + moneyEarned;

    print("Novo salario: ${newSalary.toStringAsFixed(2)}");
    print("Reajuste ganho: ${moneyEarned.toStringAsFixed(2)}");
    print("Em percentual: 10 %");
  } else if (salary >= 1200.01 && salary <= 2000.00) {
    moneyEarned = (salary * 7) / 100;
    newSalary = salary + moneyEarned;

    print("Novo salario: ${newSalary.toStringAsFixed(2)}");
    print("Reajuste ganho: ${moneyEarned.toStringAsFixed(2)}");
    print("Em percentual: 7 %");
  } else {
    moneyEarned = (salary * 4) / 100;
    newSalary = salary + moneyEarned;

    print("Novo salario: ${newSalary.toStringAsFixed(2)}");
    print("Reajuste ganho: ${moneyEarned.toStringAsFixed(2)}");
    print("Em percentual: 4 %");
  }
}
