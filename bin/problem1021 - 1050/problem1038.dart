import 'dart:io';

void main() {
  int x, y;
  var pt = {1: 4.00, 2: 4.50, 3: 5.00, 4: 2.00, 5: 1.50};

  List<String> inputValues = stdin.readLineSync()!.split(" ");
  x = int.parse(inputValues[0]);
  y = int.parse(inputValues[1]);

  double total = pt[x]! * y;

  print("Total: R\$ ${total.toStringAsFixed(2)}");
}
