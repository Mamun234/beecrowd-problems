import 'dart:io';

void main() {
  int a, b, cal;

  List<String> inputValues = stdin.readLineSync()!.split(" ");
  a = int.parse(inputValues[0]);
  b = int.parse(inputValues[1]);

  if (a > b) {
    cal = (b + 24) - a;
    print("O JOGO DUROU $cal HORA(S)");
  } else if (a == b) {
    print("O JOGO DUROU 24 HORA(S)");
  } else if (a < b) {
    cal = b - a;
    print("O JOGO DUROU $cal HORA(S)");
  }
}
