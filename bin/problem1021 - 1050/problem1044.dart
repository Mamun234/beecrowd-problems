import 'dart:io';

void main() {
  int a, b;

  List<String> inputValues = stdin.readLineSync()!.split(" ");
  a = int.parse(inputValues[0]);
  b = int.parse(inputValues[1]);

  if (a % b == 0 || b % a == 0) {
    print("Sao Multiplos");
  } else {
    print("Nao sao Multiplos");
  }
}
