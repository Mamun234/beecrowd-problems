import 'dart:io';

void main() {
  int initHour, initMinute, endHour, endMinute, calH = 0, calM;

  List<String> inputValues = stdin.readLineSync()!.split(" ");
  initHour = int.parse(inputValues[0]);
  initMinute = int.parse(inputValues[1]);
  endHour = int.parse(inputValues[2]);
  endMinute = int.parse(inputValues[3]);

  if (initMinute > endMinute) {
    endMinute = endMinute + 60;
    endHour = endHour - 1;
    calM = endMinute - initMinute;
    if (endHour < initHour) {
      endHour = endHour + 24;
      calH = endHour - initHour;
    } else if (endHour > initHour) {
      calH = endHour - initHour;
    }
    print("O JOGO DUROU $calH HORA(S) E $calM MINUTO(S)");
  } else if (initMinute < endMinute) {
    calM = endMinute - initMinute;
    if (endHour < initHour) {
      endHour = endHour + 24;
      calH = endHour - initHour;
    } else if (endHour > initHour) {
      calH = endHour - initHour;
    }
    print("O JOGO DUROU $calH HORA(S) E $calM MINUTO(S)");
  } else if (initHour == endHour && initMinute == endMinute) {
    print("O JOGO DUROU 24 HORA(S) E 0 MINUTO(S)");
  }
}
