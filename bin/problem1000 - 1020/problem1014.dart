import 'dart:io';

void main() {
  int x;
  double y, avgconsumption;

  x = int.parse(stdin.readLineSync()!);
  y = double.parse(stdin.readLineSync()!);

  avgconsumption = x / y;

  print("${avgconsumption.toStringAsFixed(3)} km/l");
}
