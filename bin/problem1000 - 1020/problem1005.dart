import 'dart:io';

void main() {
  double a, b, m;
  a = double.parse(stdin.readLineSync()!);
  b = double.parse(stdin.readLineSync()!);

  m = ((a / 11 * 3.5) + (b / 11 * 7.5));

  print(
    "MEDIA = ${m.toStringAsFixed(5)}",
  );
}
