import 'dart:io';

void main() {
  double a, b, c, m;
  a = double.parse(stdin.readLineSync()!);
  b = double.parse(stdin.readLineSync()!);
  c = double.parse(stdin.readLineSync()!);
  m = (a / 10 * 2) + (b / 10 * 3) + (c / 10 * 5);

  print("MEDIA = ${m.toStringAsFixed(1)}");
}
