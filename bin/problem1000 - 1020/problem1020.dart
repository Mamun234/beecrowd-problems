import 'dart:io';

void main() {
  int days, ano, mes, dia;
  days = int.parse(stdin.readLineSync()!);

  ano = (days ~/ 365).floor();
  int remainingDays = days % 365;
  mes = (remainingDays ~/ 30).floor();
  dia = remainingDays % 30;

  print("$ano ano(s)\n$mes mes(es)\n$dia dia(s)");
}
