import 'dart:io';

void main() {
  int n;
  List<int> banknotes = [100, 50, 20, 10, 5, 2, 1];

  n = int.parse(stdin.readLineSync()!);
  print(n);

  for (int denomination in banknotes) {
    int count = n ~/ denomination;
    if (count > -1) {
      print("$count nota(s) de R\$ $denomination,00");
      n %= denomination;
    }
  }
}
