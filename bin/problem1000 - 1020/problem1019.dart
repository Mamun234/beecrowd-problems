import 'dart:io';

void main() {
  int n, hour, minute, second;

  n = int.parse(stdin.readLineSync()!);

  hour = n ~/ 3600;
  minute = (n % 3600) ~/ 60;
  second = n % 60;

  String formattedTime =
      '${hour.toString().padLeft(1, '0')}:${minute.toString().padLeft(1, '0')}:${second.toString().padLeft(1, '0')}';

  print(formattedTime);
}
