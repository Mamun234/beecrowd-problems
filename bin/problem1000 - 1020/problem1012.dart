import 'dart:io';

void main() {
  double a, b, c;

  // Read input values
  String input = stdin.readLineSync()!;
  List<String> inputValues = input.split(' ');

  a = double.parse(inputValues[0]);
  b = double.parse(inputValues[1]);
  c = double.parse(inputValues[2]);

  double triangul = (a * c) / 2;
  print("TRIANGULO: ${triangul.toStringAsFixed(3)}");

  double circul = 3.14159 * c * c;
  print("CIRCULO: ${circul.toStringAsFixed(3)}");

  double trapezio = (((a + b) / 2) * c);
  print("TRAPEZIO: ${trapezio.toStringAsFixed(3)}");

  double square = (b * b);
  print("QUADRADO: ${square.toStringAsFixed(3)}");

  double rectangle = (a * b);
  print("RETANGULO: ${rectangle.toStringAsFixed(3)}");
}

//uncomplite run time error