import 'dart:io';

void main() {
  int hours, averageSpeed;

  String input = stdin.readLineSync()!;
  List<String> inputValue = input.split(" ");
  hours = int.parse(inputValue[0]);

  String input1 = stdin.readLineSync()!;
  List<String> inputValue1 = input1.split(" ");
  averageSpeed = int.parse(inputValue1[0]);

  double neededFuel = (hours * averageSpeed) / 12;

  print(neededFuel.toStringAsFixed(3));
}
