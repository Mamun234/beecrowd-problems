import 'dart:io';
import 'dart:math';

void main() {
  double x1, y1, x2, y2;

  // Read input values
  String input = stdin.readLineSync()!;
  List<String> inputValues = input.split(' ');
  x1 = double.parse(inputValues[0]);
  y1 = double.parse(inputValues[1]);

  String input1 = stdin.readLineSync()!;
  List<String> inputValues1 = input1.split(' ');
  x2 = double.parse(inputValues1[0]);
  y2 = double.parse(inputValues1[1]);

  double distance = sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2));

  print(distance.toStringAsFixed(4));
}

