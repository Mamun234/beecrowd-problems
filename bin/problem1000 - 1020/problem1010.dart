import 'dart:io';

void main() {
  int? productCode, productUnit;
  double? productPrice, paidAmount;

  // Read input values
  String input = stdin.readLineSync()!;
  List<String> inputValues = input.split(' ');

  productCode = int.parse(inputValues[0]);
  productUnit = int.parse(inputValues[1]);
  productPrice = double.parse(inputValues[2]);
  paidAmount = productUnit * productPrice;

  // Read input values
  String input1 = stdin.readLineSync()!;
  List<String> inputValues1 = input1.split(' ');

  productCode = int.parse(inputValues1[0]);
  productUnit = int.parse(inputValues1[1]);
  productPrice = double.parse(inputValues1[2]);
  paidAmount += productUnit * productPrice;

  print("VALOR A PAGAR: R\$ ${paidAmount.toStringAsFixed(2)}");
 
}
