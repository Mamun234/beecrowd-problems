
import 'dart:io';

void main() {
  String name;
  double fixedSalary, sales, salaryTotal;

  name = stdin.readLineSync()!;
  fixedSalary = double.parse(stdin.readLineSync()!);
  sales = double.parse(stdin.readLineSync()!);
  salaryTotal = ((sales * 15) / 100) + fixedSalary;

  print("TOTAL = R\$ ${salaryTotal.toStringAsFixed(2)}");
}
