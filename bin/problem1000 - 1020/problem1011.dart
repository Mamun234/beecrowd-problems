import 'dart:io';

void main() {
  int r;
  double pi = 3.14159, volume;

  r = int.parse(stdin.readLineSync()!);

  volume = (4 / 3) * pi * r * r * r;

  print("VOLUME = ${volume.toStringAsFixed(3)}");
}
