import 'dart:io';

void main() {
  double? A, R;
  R = double.parse(stdin.readLineSync()!);

  A = 3.14159 * R * R;

  print("A=${A.toStringAsFixed(4)}");
}


// Algorithm - 

// We know Pi = 3.14159
// Take input the radius - R
// Area = Pi * R2
// Print as float Upto 4 decimal
// Must use return with a new line \n
