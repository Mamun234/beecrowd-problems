import 'package:beecrowd_problems/beecrowd_problems.dart';
import 'package:test/test.dart';

void main() {
  test('calculate', () {
    expect(calculate(), 42);
  });
}
